import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/category_offer.dart';
import "dart:async";

import 'package:menu_app/model/product.dart';
import 'package:menu_app/supportedFile/constant.dart';


class BranchBloc implements Bloc {
  final _controller = StreamController<List<Branch>>();
  
  Stream<List<Branch>> get branchsStream => _controller.stream;

  void fetchData() async {
    _controller.sink.add(Constant.branchList);
  }
  @override
  void dispose() {
    _controller.close();
  }


}