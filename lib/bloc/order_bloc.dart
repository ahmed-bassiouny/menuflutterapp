import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/location.dart';
import "dart:async";

import 'package:menu_app/model/order.dart';
import 'package:menu_app/model/promo_code.dart';
import 'package:menu_app/supportedFile/constant.dart';
import 'package:toast/toast.dart';

class OrderBloc implements Bloc {
  final _loadingController = StreamController<bool>();
  final _infoController = StreamController<OrderInfo>();

  Stream<bool> get isLoading => _loadingController.stream;
  Stream<OrderInfo> get info => _infoController.stream;
  OrderInfo _orderInfo;
  LatLng _location;
  BuildContext _context;

  OrderBloc(BuildContext context, double total) {
    _context = context;
    _orderInfo = OrderInfo();
    _orderInfo.subTotal = total;
    _infoController.sink.add(_orderInfo);
  }
  void makeOrder(String name, String address, String phone, LatLng location) async {
    Order order = Order(name, phone);
    order.location = Location(address, location.latitude, location.longitude);
    // convert from cart item to order item
    List<OrderItem> orderItems = new List<OrderItem>();
    for (var item in Constant.cartList) {
      orderItems.add(OrderItem(item.id, item.count, item.size,item.isOffer));
    }
    order.items = orderItems;
    order.deviceType = Constant.DEVICE_TYPE;
    order.deviceId = Constant.DEVICE_ID;
    if (_orderInfo.branchSelected == null){
      Toast.show("Please select branch", _context);
      return;
    }
    order.branchId = _orderInfo.branchSelected.id.toString();
    order.deliveryCharge = _orderInfo.deliveryCharge.toString();
    if (_orderInfo.promoCode != null){
      order.promocode = _orderInfo.promoCode.code.toString();
    }else {
      order.promocode = "";
    }
    _startLoading();
    BaseResponse response = await ApiRequest.makeOrder(order);
    _stopLoading();
    if (response.status) {
      Constant.cartList = [];
      Navigator.of(_context).pop();
      Navigator.of(_context).pop();
    } else {
      Toast.show(response.message, _context);
    }
    
  }

  void _startLoading() {
    _loadingController.sink.add(true);
  }

  void _stopLoading() {
    _loadingController.sink.add(false);
  }

  @override
  void dispose() {
    _loadingController.close();
    _infoController.close();
  }

  void getLocation(LatLng value) async {
    _location = value;
    var address = "";
    List<Placemark> placemark = await Geolocator()
        .placemarkFromCoordinates(value.latitude, value.longitude);
    if (placemark[0] != null) {
      address = "${placemark[0].name} , ${placemark[0].subLocality} ";
    }
    _orderInfo.addressSelected = address;
    _infoController.sink.add(_orderInfo);
    calculateDeliveryPrice();
  }

  void selectedBranch(Branch item) async {
    _orderInfo.branchSelected = item;
    _infoController.sink.add(_orderInfo);
    calculateDeliveryPrice();
  }

  void calculateDeliveryPrice() async {
    if (_location == null || _orderInfo.branchSelected == null) return;
    BaseResponse<double> response = await ApiRequest.getCharge(
        _orderInfo.branchSelected.id, _location.latitude, _location.longitude);

    if (response.status) {
      _orderInfo.deliveryCharge = response.data;
      _infoController.sink.add(_orderInfo);
    } else {
      Toast.show(response.message, _context);
    }
  }

  void setPromoCode(PromoCode promoCode) {
    _orderInfo.promoCode = promoCode;

    _infoController.sink.add(_orderInfo);
  }
}

class OrderInfo {
  String addressSelected;
  Branch branchSelected;
  double subTotal;

  PromoCode promoCode;
  double deliveryCharge = 0;

  double getTotal() {
    return subTotal +
        deliveryCharge +
        -((subTotal * int.parse(promoCode == null ? "0" : promoCode.discount)) /
            100);
  }
}
