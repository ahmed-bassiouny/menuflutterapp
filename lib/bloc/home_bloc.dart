import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/category_offer.dart';
import "dart:async";


class HomeBloc implements Bloc {
  final _controller = StreamController<CategoryOffer>();
  
  Stream<CategoryOffer> get homeStream => _controller.stream;

  void fetchData() async {
    BaseResponse<CategoryOffer> result = await ApiRequest.getHome();
    if (result.status){
      _controller.sink.add(result.data);
    }else {
      _controller.sink.addError(result.message);
    }
    
  }

  @override
  void dispose() {
    _controller.close();
  }


}