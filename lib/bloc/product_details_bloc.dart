import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/cart_item.dart';
import "dart:async";


class ProductDetailsBloc implements Bloc {
  final _controller = StreamController<CartItem>();


  var item = CartItem(count: 1,price: 0,title: "");

  void setInfo(int id,String title,String image,bool isOffer){
    item.id = id;
    item.title = title;
    item.image = image;
    item.isOffer = isOffer;
  }
  void changeSize(double price, String size) {
    item.price = price;
    item.size = size;
    item.count = 1; 
    _controller.sink.add(item);
  }

  Stream<CartItem> get productDetailsStream => _controller.stream;

  void increament() {
    item.count++;
    _controller.sink.add(item);
  }

  void decreament() {
    if (item.count == 1) return;
    item.count--;
    _controller.sink.add(item);
  }

  @override
  void dispose() {
    _controller.close();
  }
}

