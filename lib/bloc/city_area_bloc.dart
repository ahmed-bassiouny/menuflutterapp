import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/area.dart';

import 'package:menu_app/model/city.dart';
import 'package:menu_app/supportedFile/constant.dart';
import "dart:async";

import 'package:shared_preferences/shared_preferences.dart';

class CityAreaBloc implements Bloc {
  final _controller = StreamController<CityAreaBlocHelper>();

  Stream<CityAreaBlocHelper> get stream => _controller.stream;

  CityAreaBlocHelper item = CityAreaBlocHelper();

  void fetchCityData() async {
    BaseResponse<List<City>> result = await ApiRequest.getCity();
    _stopLoading();

    if (result.status) {
      item.cityList = result.data;
      _controller.sink.add(item);
    } else {
      _controller.sink.addError(result.message);
    }
  }

  void fetchAreaData() async {
    if (item.city == null) return;
    BaseResponse<List<Area>> result = await ApiRequest.getArea(item.city.id);
    _stopLoading();
    if (result.status) {
      item.areaList = result.data;
      _controller.sink.add(item);
    } else {
      _controller.sink.addError(result.message);
    }
  }

  void startLoading() {
    item.loading = true;
    _controller.sink.add(item);
  }

  void _stopLoading() {
    item.loading = false;
  }

  @override
  void dispose() {
    _controller.close();
  }

  void setCity(City city) {
    item.city = city;
    _controller.sink.add(item);
  }

  void setArea(Area area) {
    item.area = area;
    _controller.sink.add(item);
  }

  void save(BuildContext context) async {
    // this case mean select area page open firat time
    var openHome = Constant.REGION == null;
    if (item.area != null && item.city != null) {
      startLoading();
      final prefs = await SharedPreferences.getInstance();
      prefs.setString(Constant.REGION_KEY, jsonEncode(item.area));
      Constant.REGION = item.area;
      if (openHome) {
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/main', (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pop();
      }
    }
  }
}

class CityAreaBlocHelper {
  List<City> cityList;
  List<Area> areaList;
  bool loading;
  City city;
  Area area;
}
