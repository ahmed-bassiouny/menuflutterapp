import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/cart_item.dart';
import "dart:async";

import 'package:menu_app/supportedFile/constant.dart';

class CartBloc implements Bloc {
  final _controller = StreamController<List<CartItem>>();
  final _totalController = StreamController<double>();
  List list;
  var total = 0.0;

  CartBloc() {
    list = Constant.cartList;
    for (CartItem item in list) {
      total += item.total;
    }
    _controller.sink.add(list);
    _totalController.sink.add(total);
  }

  Stream<List<CartItem>> get cartStream => _controller.stream;
  Stream<double> get totalStream => _totalController.stream;

  void increament(var index) {
    CartItem item = list[index];
    item.count++;
    total += item.price;
    list[index] = item;
    _controller.sink.add(list);
    _totalController.sink.add(total);
  }

  void decreament(var index) {
    CartItem item = list[index];
    if (item.count == 1) {
      total -= item.price;
      list.remove(item);
      _controller.sink.add(list);
      _totalController.sink.add(total);
    } else {
      item.count--;
      total -= item.price;
      list[index] = item;
      _controller.sink.add(list);
      _totalController.sink.add(total);
    }
  }

  @override
  void dispose() {
    _controller.close();
    _totalController.close();
  }
}
