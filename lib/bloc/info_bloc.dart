import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/about.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/category_offer.dart';
import 'package:menu_app/model/info.dart';
import "dart:async";

import 'package:menu_app/model/product.dart';


class InfoBloc implements Bloc {
  final _controller = StreamController<AboutUsWithInfo>();
  
  Stream<AboutUsWithInfo> get infoStream => _controller.stream;

  AboutUsWithInfo item = AboutUsWithInfo();

  void fetchData() async {
    BaseResponse<Info> result = await ApiRequest.getInfo();
    if (result.status){
      item.info = result.data;
      _controller.sink.add(item);
      _aboutUs();
    }else {
      _controller.sink.addError(result.message);
    }
  }

  void _aboutUs() async {
    BaseResponse<About> result = await ApiRequest.getAbout();
    if (result.status){
      item.about = result.data;
      _controller.sink.add(item);
    }else {
      _controller.sink.addError(result.message);
    }
  }
  @override
  void dispose() {
    _controller.close();
  }
}

class AboutUsWithInfo{

  Info info;
  About about;
}