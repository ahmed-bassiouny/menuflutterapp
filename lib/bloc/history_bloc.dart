import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/category_offer.dart';
import 'package:menu_app/model/order.dart';
import "dart:async";

import 'package:menu_app/model/product.dart';
import 'package:menu_app/supportedFile/constant.dart';


class HistoryBloc implements Bloc {
  final _controller = StreamController<List<Order>>();
  
  Stream<List<Order>> get ordersStream => _controller.stream;

  void fetchData() async {
    BaseResponse<List<Order>> result = await ApiRequest.getHistory(Constant.DEVICE_ID);
    if (result.status){
      _controller.sink.add(result.data);
    }else {
      _controller.sink.addError(result.message);
    } 
  }
  @override
  void dispose() {
    _controller.close();
  }


}