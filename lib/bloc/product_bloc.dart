import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/category_offer.dart';
import "dart:async";

import 'package:menu_app/model/product.dart';


class ProductBloc implements Bloc {
  final _controller = StreamController<List<Product>>();
  
  Stream<List<Product>> get productsStream => _controller.stream;

  void fetchData(int categoryId) async {
    BaseResponse<List<Product>> result = await ApiRequest.getProductList(categoryId);
    if (result.status){
      _controller.sink.add(result.data);
    }else {
      _controller.sink.addError(result.message);
    }
    
  }

  @override
  void dispose() {
    _controller.close();
  }


}