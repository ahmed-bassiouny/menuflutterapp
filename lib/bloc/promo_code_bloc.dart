import 'dart:async';

import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/promo_code.dart';
import 'package:menu_app/supportedFile/constant.dart';


class PromoCodeBloc implements Bloc {
  final _controller = StreamController<PromoCode>();
  final _loadingController = StreamController<bool>();

  Stream<PromoCode> get stream => _controller.stream;
  Stream<bool> get loading => _loadingController.stream;
  
  PromoCodeBloc(){
    _loadingController.sink.add(false);
  }

  void checkPromoCode(BuildContext context,String promoCode) async {
    _loadingController.sink.add(true);
    BaseResponse<PromoCode> result = await ApiRequest.checkPromoCode(promoCode);
    _loadingController.sink.add(false);
    if (result.status){
      _controller.sink.add(result.data);
      Navigator.of(context).pop(result.data);
    }else {
      _controller.sink.addError(result.message);
    }
    
  }
  @override
  void dispose() {
    _controller.close();
  }
}
