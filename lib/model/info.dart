class Info {
  String name;
  String image;
  String address;
  double latitude;
  double longitude;
  String facebook;
  String instgram;
  Phone phones;

  Info.fromJson(Map<String, dynamic> json)
      : name = json["name"] == null ? "" : json["name"],
        image = json["logo"] == null ? "" : json["logo"],
        latitude = json["latitude"] == null ? 0 : json["latitude"],
        longitude = json["longitude"] == null ? 0 : json["longitude"],
        facebook = json["facebook"] == null ? "" : json["facebook"],
        instgram = json["instgram"] == null ? "" : json["instgram"],
        phones = Phone.fromJson(json["phones"]),
        address = json["address"] == null ? "" : json["address"];
}

class Phone {
  String phone1;
  String phone2;

  Phone.fromJson(Map<String, dynamic> json)
      : phone1 = json["phone1"] == null ? "" : json["phone1"],
        phone2 = json["phone2"] == null ? "" : json["phone2"];
}
