import 'dart:ffi';

import 'package:dio/dio.dart';
import 'package:menu_app/model/about.dart';
import 'package:menu_app/model/api/api_config.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/area.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/category_offer.dart';
import 'package:menu_app/model/city.dart';
import 'package:menu_app/model/info.dart';
import 'package:menu_app/model/order.dart';
import 'package:menu_app/model/product.dart';
import 'package:menu_app/model/promo_code.dart';
import 'package:menu_app/model/social_media.dart';
import 'package:menu_app/supportedFile/base_info.dart';

class ApiRequest {
  ApiConfig config = ApiConfig();

  static Future<BaseResponse<CategoryOffer>> getHome() async {
    Response response = await ApiConfig.dio.get("home-screen");
    BaseResponse<CategoryOffer> result =
        BaseResponse<CategoryOffer>.fromJson(response.data);
      if (result.status){
      CategoryOffer item = CategoryOffer.fromJson(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<List<Branch>>> getBranches() async {
    Response response = await ApiConfig.dio.get("branches");
    BaseResponse<List<Branch>> result =
        BaseResponse<List<Branch>>.fromJson(response.data);
      if (result.status){
      List<Branch> item = Branch.fromList(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<About>> getAbout() async {
    Response response = await ApiConfig.dio.get("about");
    BaseResponse<About> result =
        BaseResponse<About>.fromJson(response.data);
      if (result.status){
      About item = About.fromJson(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<Info>> getInfo() async {
    Response response = await ApiConfig.dio.get("info");
    BaseResponse<Info> result =
        BaseResponse<Info>.fromJson(response.data);
      if (result.status){
      Info item = Info.fromJson(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<SocialMedia>> getSocialMedia() async {
    Response response = await ApiConfig.dio.get("social-media");
    BaseResponse<SocialMedia> result =
        BaseResponse<SocialMedia>.fromJson(response.data);
      if (result.status){
      SocialMedia item = SocialMedia.fromJson(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<List<Product>>> getProductList(int categoryId) async {
    Response response = await ApiConfig.dio.get("category/$categoryId/items");
    BaseResponse<List<Product>> result =
        BaseResponse<List<Product>>.fromJson(response.data);
      if (result.status){
      List<Product> item = Product.fromList(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<double>> getCharge(int branchId,double lat , double lng) async {
    Response response = await ApiConfig.dio.post("customer/orders/delivery-charge",queryParameters: {
      "latitude":lat,
      "longitude":lng,
      "branch_id":branchId,
    });
    
    BaseResponse<double> result =
        BaseResponse<double>.fromJson(response.data);
      if (result.status){
      double item = double.parse(response.data['data']['delivery_charge'].toString());
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<List<Order>>> getHistory(String deviceId) async {
    Response response = await ApiConfig.dio.get("customer/orders/history/$deviceId");
    BaseResponse<List<Order>> result =
        BaseResponse<List<Order>>.fromJson(response.data);
      if (result.status){
      List<Order> item = Order.fromList(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse> makeOrder(Order order) async {
    Response response = await ApiConfig.dio.post("customer/orders/store",queryParameters: order.toJson());
    BaseResponse result =
        BaseResponse.fromJson(response.data);
    return result;
     
   }

   static Future<BaseResponse<List<City>>> getCity() async {
    Response response = await ApiConfig.dio.get("cities");
    BaseResponse<List<City>> result =
        BaseResponse<List<City>>.fromJson(response.data);
      if (result.status){
      List<City> item = City.fromList(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<List<Area>>> getArea(int cityId) async {
    Response response = await ApiConfig.dio.get("city/$cityId/delivery-regions");
    BaseResponse<List<Area>> result =
        BaseResponse<List<Area>>.fromJson(response.data);
      if (result.status){
      List<Area> item = Area.fromList(result.temp);
      result.data = item;
    }
    return result;
  }

  static Future<BaseResponse<PromoCode>> checkPromoCode(String promoCode) async {
    Response response = await ApiConfig.dio.get("promo-code/$promoCode");
    BaseResponse<PromoCode> result =
        BaseResponse<PromoCode>.fromJson(response.data);
      if (result.status){
      PromoCode item = PromoCode.fromJson(result.temp);
      result.data = item;
    }
    return result;
  }
}
