import 'package:dio/dio.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:menu_app/supportedFile/base_info.dart';
import 'package:menu_app/supportedFile/constant.dart';

class ApiConfig {

  static Map<String, dynamic> _header = {
    BaseInfo.TOKEN_KEY: BaseInfo.TOKEN
  };

  

  static final Dio dio = new Dio(BaseOptions(
    baseUrl: BaseInfo.BASE_URL,
    connectTimeout: 25000,
    sendTimeout: 20000,
    headers:_header,
    queryParameters: {"lang": Constant.locale}
  
  ));
}