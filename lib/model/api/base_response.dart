class BaseResponse<T>{
  bool status ;
  String message;
  dynamic temp;
  T data;

  
  BaseResponse(this.status, this.message);

  

  BaseResponse.fromJson(Map json)
      : status = json["status"],
        message = json["message"],
        temp = json["data"] == null ? "" : json["data"];
}