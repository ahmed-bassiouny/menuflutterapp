import 'package:menu_app/model/location.dart';

class Order {
  int id;
  String name;
  String phone;
  String address;
  List<OrderItem> items;
  Location location;
  String deviceId;
  String deviceType;
  String promocode;
  String branchId;
  String deliveryCharge;

  Order(name,phone){
    this.name = name;
    this.phone = phone;
  }
  Order.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        name = json["name"] == null ? "" : json["name"],
        phone = json["phone"] == null ? "" : json["phone"],
        address = json["address"] == null ? "" : json["address"],
        items = OrderItem.fromList(json['items']);

  Map<String, dynamic> toJson() => {
        "name": name,
        "phone": phone,
        "device_id": deviceId,
        "device_type": deviceType,
        "branch_id": branchId,
        "delivery_charge": deliveryCharge,
        "promocode": promocode,
        "location": location.toJson(),
        "cart":OrderItem.toMapList(items)
      };

  static List fromList(List list) {
    return list.map((item) {
      return Order.fromJson(item);
    }).toList();
  }
}

class OrderItem {
  int id;
  String name;
  int qty;
  String option;
  int totalPrice;
  bool isOffer;

  OrderItem(int id,int qty,String option,bool isOffer){
    this.id = id;
    this.qty=qty;
    this.option = option;
    this.isOffer = isOffer;
  }

  OrderItem.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        name = json["name"] == null ? "" : json["name"],
        option = json["name"] == null ? "" : json["option"],
        qty = json["qty"] == null ? 0 : json["qty"],
        totalPrice = json["total_price"] == null ? 0 : json["total_price"];

  static List fromList(List list) {
    return list.map((item) {
      return OrderItem.fromJson(item);
    }).toList();
  }

  static List<Map<String, dynamic>> toMapList(List<OrderItem> list) {
    return list.map((item) {
      return item.toJson();
    }).toList();
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "qty": qty,
        "option": option,
        "is_offer": isOffer
      };
}
