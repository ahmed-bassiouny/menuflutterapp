class Offer {
  String image;

  Offer.fromJson(Map<String, dynamic> json)
      : image = json["image"] == null ? "" : json["image"];

  static List fromList(List list) {
    return list.map((item) {
      return Offer.fromJson(item);
    }).toList();
  }
}
