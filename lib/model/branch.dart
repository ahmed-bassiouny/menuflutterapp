import 'package:menu_app/model/location.dart';

class Branch {
  int id;
  Location location;
  String city;
  String name;

  

  Branch.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        location = Location.fromJson(json["location"]),
        city = json["city"] == null ? "" : json["city"],
        name = json["name"] == null ? "" : json["name"];

  static List fromList(List list) {
    return list.map((item) {
      return Branch.fromJson(item);
    }).toList();
  }
}
