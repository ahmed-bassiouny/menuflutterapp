import 'package:menu_app/model/category.dart';
import 'package:menu_app/model/offer.dart';
import 'package:menu_app/model/product.dart';

class CategoryOffer {
  List<Product> offers;
  List<Category> categories;

  CategoryOffer.fromJson(Map json)
      : offers = Product.fromList(json["offers"]),
        categories = Category.fromList(json["categories"]) ;
}