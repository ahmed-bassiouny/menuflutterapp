
class Product {
  int id;
  String name;
  String description;
  Map<String, dynamic> options;
  double price;
  String image;
  bool isOffer;

  Product.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        name = json["name"] == null ? "" : json["name"],
        description = json["description"] == null ? "" : json["description"],
        options = json['options'],
        price = json["price"] == null ? 0 : json["price"],
        image = json["image"] == null ? "" : json["image"],
        isOffer = json["is_offer"] == null ? false : json["is_offer"];

  static List fromList(List list) {
    return list.map((item) {
      return Product.fromJson(item);
    }).toList();
  }
}
