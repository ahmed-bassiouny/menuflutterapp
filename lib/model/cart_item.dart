class CartItem{
  int id;
  double price;
  String size;
  int count;
  int _total;
  String title;
  String image;
  bool isOffer;

  CartItem({this.count,this.price,this.title,this.isOffer});

  double get total => price * count;
  

}