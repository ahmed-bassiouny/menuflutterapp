class About {
  String title;
  String image;
  String text;

    About.fromJson(Map<String, dynamic> json)
      : title = json["title"] == null ? "" : json["title"],
        image = json["image"] == null ? "" : json["image"],
        text = json["text"] == null ? "" : json["text"];
}