class Category {
  int id;
  String name;
  String image;

  Category.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        name = json["name"] == null ? "" : json["name"],
        image = json["image"] == null ? "" : json["image"];

  static List fromList(List list) {
    return list.map((item) {
      return Category.fromJson(item);
    }).toList();
  }
}
