class Area {
  int id;
  String name;
  int cost;

  Area.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        name = json["name"] == null ? "" : json["name"],
        cost = json["charge"] == null ? 0 : json["charge"];

  static List fromList(List list) {
    return list.map((item) {
      return Area.fromJson(item);
    }).toList();
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "charge": cost,
      };
}
