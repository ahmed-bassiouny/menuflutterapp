class City {
  int id;
  String name;

  City.fromJson(Map<String, dynamic> json)
      : id = json["id"] == null ? 0 : json["id"],
        name = json["name"] == null ? "" : json["name"];

  static List fromList(List list) {
    return list.map((item) {
      return City.fromJson(item);
    }).toList();
  }
}
