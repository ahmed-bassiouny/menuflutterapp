class Location {
  double latitude;
  double longitude;
  String address;

  Location(String address,double lat,double lng){
    this.address = address;
    this.latitude = lat;
    this.longitude = lng;
  }
  Location.fromJson(Map<String, dynamic> json)
      : latitude = json["latitude"] == null ? 0 : json["latitude"],
        longitude = json["longitude"] == null ? 0 : json["longitude"],
        address = json["address"] == null ? "" : json["address"];

  Map<String, dynamic> toJson() => {
        "latitude": latitude,
        "longitude": longitude,
        "address": address,
      };
}
