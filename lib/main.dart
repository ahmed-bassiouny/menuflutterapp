import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:menu_app/model/about.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/product.dart';
import 'package:menu_app/supportedFile/constant.dart';
import 'package:menu_app/ui/pages/about_page.dart';
import 'package:menu_app/ui/pages/branch_page.dart';
import 'package:menu_app/ui/pages/cart_page.dart';
import 'package:menu_app/ui/pages/category_page.dart';
import 'package:menu_app/ui/pages/history_page.dart';
import 'package:menu_app/ui/pages/map_screen.dart';
import 'package:menu_app/ui/pages/order_page.dart';
import 'package:menu_app/ui/pages/product_details.dart';
import 'package:menu_app/ui/pages/product_list.dart';
import 'package:menu_app/ui/pages/promo_code_page.dart';
import 'package:menu_app/ui/pages/select_area_page.dart';
import 'package:menu_app/ui/pages/splash.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'supportedFile/app_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  MaterialColor myColor = const MaterialColor(
    0xFFa71f24,
    const <int, Color>{
      50: const Color(0xFFa71f24),
      100: const Color(0xFFFFFFFF),
      200: const Color(0xFFFFFFFF),
      300: const Color(0xFFFFFFFF),
      400: const Color(0xFFFFFFFF),
      500: const Color(0xFFFFFFFF),
      600: const Color(0xFFFFFFFF),
      700: const Color(0xFFFFFFFF),
      800: const Color(0xFFFFFFFF),
      900: const Color(0xFFFFFFFF),
    },
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: myColor,
            accentColor: myColor),
        supportedLocales: [Locale('en', 'US'), Locale('ar', 'AR')],
        localizationsDelegates: [
          //class which loads the translation from json files
          AppLocalizations.delegate,
          // built in localization for material widget
          GlobalMaterialLocalizations.delegate,
          // built in localization for text directions ltr/rtl
          GlobalWidgetsLocalizations.delegate,
        ],
        // return local which will be used by the app
        localeResolutionCallback: (local, supportedLocales) {
          if (local != null && local.languageCode.contains("ar")) {
            Constant.locale = "ar";
            return supportedLocales.last;
          }
          return supportedLocales.first;
        },
        home: SplashPage(),
        debugShowCheckedModeBanner: false,
        routes: {
          '/main': (context) => CategoryPage(),
          '/select_area': (context) => SelectAreaPage(),
          '/cart': (context) => CartPage(),
          '/branch': (context) => BranchPage(),
          '/about': (context) => AboutPage(),
          '/history': (context) => HistoryPage(),
          '/promo_code': (context) => PromoCodePage(),
          '/map': (context) => MapScreen(),
        },
        onGenerateRoute: (RouteSettings settings) {
          if (settings.name == '/product_list') {
            return MaterialPageRoute(builder: (_) {
              Map map = Map.from(settings.arguments);
              return ProductList(
                id: map['id'],
                title: map["title"],
              );
            });
          } else if (settings.name == '/product_details') {
            return MaterialPageRoute(builder: (_) {
              Map map = Map.from(settings.arguments);
              return ProductDetails(
                item: map['item'],
              );
            });
          } else if (settings.name == '/order') {
            return MaterialPageRoute(builder: (_) {
              Map map = Map.from(settings.arguments);
              return OrderPage(
                total: map['item'],
              );
            });
          }
        });
  }
}
