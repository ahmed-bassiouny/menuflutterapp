import 'package:flutter/material.dart';
import 'package:menu_app/supportedFile/constant.dart';

class MyAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  const MyAppbar({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      actions: <Widget>[
        Stack(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).pushNamed("/cart");
              },
            ),
            Constant.cartList.length > 0 ?
            Container(
              width: 10,
              height: 10,
              margin: EdgeInsets.all(10),
              decoration: new BoxDecoration(
                color: Colors.orange,
                shape: BoxShape.circle,
              ),
            ):Container()
          ],
        )
      ],
    );
  }

  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
