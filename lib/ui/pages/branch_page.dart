import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/branch_bloc.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class BranchPage extends StatefulWidget {
  @override
  _BranchPageState createState() => _BranchPageState();
}

class _BranchPageState extends State<BranchPage> {
  final bloc = BranchBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BranchBloc>(
      bloc: bloc,
      child: Scaffold(
          appBar: AppBar(
            title: Text(AppLocalizations.of(context).translate("branchs")),
          ),
          body: StreamBuilder<List<Branch>>(
            stream: bloc.branchsStream,
            builder: (ctx, snapshot) {
              return snapshot.data == null
                  ? CircularProgressIndicator()
                  : ListView.builder(
                      itemCount:
                          snapshot.data == null ? 0 : snapshot.data.length,
                      itemBuilder: (ctx, index) {
                        return Card(
                          
                          child: Container(
                            
                            padding: EdgeInsets.all(8),
                            child: ListTile(
                              title: Text(snapshot.data[index].name),
                              subtitle:
                                  Text(snapshot.data[index].location.address),
                              trailing: FlatButton(
                                child: Text(
                                  "View on map",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                                onPressed: () async {
                                  final url =
                                      'https://www.google.com/maps/search/?api=1&query=${snapshot.data[index].location.latitude},${snapshot.data[index].location.longitude}';
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    throw 'Could not launch';
                                  }
                                },
                              ),
                            ),
                          ),
                        );
                      });
            },
          )),
    );
  }
}
