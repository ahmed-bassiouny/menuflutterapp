import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/order_bloc.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/promo_code.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:menu_app/supportedFile/constant.dart';
import 'package:toast/toast.dart';

class OrderPage extends StatefulWidget {
  final double total;

  OrderPage({this.total});

  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  final _formKey = GlobalKey<FormState>();
  String name, phone, address;
  var bloc;
  var textController = new TextEditingController();
  var location;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = OrderBloc(context, widget.total);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OrderBloc>(
        bloc: bloc,
        child: Scaffold(
            appBar: AppBar(
              title: Text(AppLocalizations.of(context).translate("submit")),
            ),
            body: SingleChildScrollView(
              child: Container(
                  margin: EdgeInsets.all(20),
                  child: StreamBuilder<OrderInfo>(
                      stream: bloc.info,
                      builder: (ctx, snapshot) {
                        if (snapshot.data == null) {
                          return Container();
                        }
                        if (textController != null) {
                          textController.text = snapshot.data.addressSelected;
                        }
                        return Form(
                          key: _formKey,
                          child: Wrap(
                            runSpacing: 20,
                            children: <Widget>[
                              Center(
                                child: Text(
                                  AppLocalizations.of(context).translate("complete_information"),
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                              TextFormField(
                                validator: (String str) =>
                                    str.isEmpty ? AppLocalizations.of(context).translate("invalid_name") : null,
                                onSaved: (String str) => name = str,
                                decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("name")),
                              ),
                              TextFormField(
                                validator: (String str) =>
                                    str.length != 11 || !str.startsWith("01")
                                        ? AppLocalizations.of(context).translate("invalid_phone")
                                        : null,
                                onSaved: (String str) => phone = str,
                                keyboardType: TextInputType.phone,
                                decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("phone")),
                              ),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                readOnly: true,
                                controller: textController,
                                maxLines: 2,
                                validator: (String str) => str.length < 10
                                    ? AppLocalizations.of(context).translate("invalid_address")
                                    : null,
                                onSaved: (String str) => address = str,
                                decoration:
                                    InputDecoration(labelText: AppLocalizations.of(context).translate("address")),
                                onTap: () {
                                  Navigator.of(context)
                                      .pushNamed("/map")
                                      .then((onval) {
                                    location = onval as LatLng;

                                    if (location != null) {
                                      bloc.getLocation(location);
                                    }
                                  });
                                },
                              ),
                              DropdownButton<Branch>(
                                isExpanded: true,
                                items: Constant.branchList.map((Branch value) {
                                  return new DropdownMenuItem<Branch>(
                                      value: value, child: Text(value.name));
                                }).toList(),
                                onChanged: (newValue) {
                                  bloc.selectedBranch(newValue);
                                },
                                value: snapshot.data == null
                                    ? null
                                    : snapshot.data.branchSelected,
                                hint: Text("${AppLocalizations.of(context).translate("select_branch")}"),
                              ),
                              Card(
                                margin: EdgeInsets.all(6),
                                child: Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      Text(
                                        "${AppLocalizations.of(context).translate("sub_total")} : ${snapshot.data.subTotal}",
                                        style: TextStyle(fontSize: 18),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 12),
                                      ),
                                      Text(
                                          "${AppLocalizations.of(context).translate("delivery")} : ${snapshot.data.deliveryCharge}",
                                          style: TextStyle(fontSize: 18)),
                                      Padding(
                                        padding: EdgeInsets.only(top: 18),
                                      ),
                                      snapshot.data.promoCode != null
                                          ? Text(
                                              "${AppLocalizations.of(context).translate("promo_code")} : ${snapshot.data.promoCode.discount} %",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                            )
                                          : FlatButton(
                                              child: Text(
                                                AppLocalizations.of(context).translate("enter_promo_code"),
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              ),
                                              onPressed: () {
                                                Navigator.of(context)
                                                    .pushNamed("/promo_code")
                                                    .then((onValue) {
                                                  if (onValue != null) {
                                                    var item =
                                                        onValue as PromoCode;
                                                    bloc.setPromoCode(item);
                                                  }
                                                });
                                              },
                                            ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 18),
                                      ),
                                      Center(
                                          child: Text(
                                              "${AppLocalizations.of(context).translate("total")} : ${snapshot.data.getTotal()}",
                                              style: TextStyle(fontSize: 22)))
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      })),
            ),
            floatingActionButton: StreamBuilder<bool>(
              stream: bloc.isLoading,
              builder: (ctx, snapshot) {
                return (snapshot.data != null && snapshot.data)
                    ? CircularProgressIndicator()
                    : FloatingActionButton.extended(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            bloc.makeOrder(name, address, phone, location);
                          }
                        },
                        icon: Icon(Icons.check),
                        label: Text(AppLocalizations.of(context).translate("submit")),
                      );
              },
            )));
  }
}
