import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:menu_app/model/api/api_request.dart';
import 'package:menu_app/model/api/base_response.dart';
import 'package:menu_app/model/area.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/supportedFile/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatelessWidget {
  BuildContext context;

  SplashPage() {
    loadData();
  }

  loadData() async {
    BaseResponse<List<Branch>> result = await ApiRequest.getBranches();
    if (result.status) {
      Constant.branchList = result.data;
      getKey();
    }
  }

  getKey() async {
    final prefs = await SharedPreferences.getInstance();

    Constant.DEVICE_ID = prefs.getString(Constant.DEVICE_ID_KEY);

    if (Constant.DEVICE_ID == null || Constant.DEVICE_ID.isEmpty) {
      Constant.DEVICE_ID = await Constant.getDeviceDetails();
      prefs.setString(Constant.DEVICE_ID_KEY, Constant.DEVICE_ID);
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/main', (Route<dynamic> route) => false);
    } else {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/main', (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset("images/logo.png"),
      ),
    );
  }
}
