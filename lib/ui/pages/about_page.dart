import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/info_bloc.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  final bloc = InfoBloc();

  @override
  void initState() {
    super.initState();
    bloc.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<InfoBloc>(
      bloc: bloc,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate("about")),
        ),
        body: Center(
          child: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: StreamBuilder<AboutUsWithInfo>(
                stream: bloc.infoStream,
                builder: (ctx, snapshot) {
                  if (snapshot.data == null) {
                    return CircularProgressIndicator();
                  }
                  return Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                      ),
                      Text(
                        snapshot.data.info.name,
                        style: TextStyle(fontSize: 20),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                      ),
                      Text(
                        snapshot.data.info.phones.phone1,
                        style: TextStyle(fontSize: 18),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 4),
                      ),
                      Text(
                        snapshot.data.info.address,
                        style: TextStyle(fontSize: 16),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                      ),
                      Image.network(
                        snapshot.data.info.image,
                        width: 200,
                        height: 200,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                      ),
                      Text(
                        snapshot.data.about != null
                            ? snapshot.data.about.text
                            : "",
                        style: TextStyle(fontSize: 16),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          IconButton(
                            icon: Image.asset("images/facebook1.png"),
                            iconSize: 10,
                            onPressed: () async{
                              if (snapshot.data.info.facebook.isNotEmpty) {
                                if (await canLaunch(
                                    snapshot.data.info.facebook)) {
                                  await launch(snapshot.data.info.facebook);
                                } else {
                                  throw 'Could not launch';
                                }
                              }
                            },
                          ),
                          IconButton(
                            icon: Image.asset("images/insta.png"),
                            iconSize: 10,
                            onPressed: () async {
                              if (snapshot.data.info.instgram.isNotEmpty) {
                                if (await canLaunch(
                                    snapshot.data.info.instgram)) {
                                  await launch(snapshot.data.info.instgram);
                                } else {
                                  throw 'Could not launch';
                                }
                              }
                            },
                          ),
                        ],
                      )
                    ],
                  );
                },
              )),
        ),
      ),
    );
  }
}
