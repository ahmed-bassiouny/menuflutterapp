import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/promo_code_bloc.dart';
import 'package:menu_app/model/promo_code.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:toast/toast.dart';

class PromoCodePage extends StatefulWidget {
  @override
  _PromoCodePageState createState() => _PromoCodePageState();
}

class _PromoCodePageState extends State<PromoCodePage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  final bloc = PromoCodeBloc();
  String code;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate("promo_code")),
        ),
        body: Center(
          child: StreamBuilder<PromoCode>(
            stream: bloc.stream,
            builder: (ctx, snapshot) {
              return ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 30),
                  ),
                  Image.asset(
                    "images/logo.png",
                    width: 200,
                    height: 200,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15),
                  ),
                  Container(
                    width: 300,
                    child: TextField(
                      onChanged: (String str) => code = str,
                      decoration: new InputDecoration(
                        hintText: AppLocalizations.of(context).translate("enter_promo_code"),
                        errorText: (snapshot != null && snapshot.error != null)
                            ? snapshot.error
                            : null,
                        
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 40),
                  ),
                  StreamBuilder<bool>(
                    stream: bloc.loading,
                    builder: (ctx, snapshot) {
                      if (snapshot == null ||
                          snapshot.data == null ||
                          snapshot.data)
                        return CircularProgressIndicator();
                      else
                        return FlatButton(
                          child: Text(AppLocalizations.of(context).translate("submit")),
                          onPressed: () {
                            bloc.checkPromoCode(context,code);
                          },
                        );
                    },
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
