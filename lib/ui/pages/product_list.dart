import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/product_bloc.dart';
import 'package:menu_app/model/product.dart';
import 'package:menu_app/ui/pages/product_details.dart';
import 'package:menu_app/ui/supportedUI/MyAppbar.dart';

class ProductList extends StatefulWidget {
  final int id;
  final String title;

  ProductList({this.id, this.title});

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  final bloc = ProductBloc();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc.fetchData(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: bloc,
      child: Scaffold(
          appBar: MyAppbar(
            title: widget.title,
          ),
          body: Container(
            child: StreamBuilder<List<Product>>(
              stream: bloc.productsStream,
              builder: (cxt, snapshot) {
                if (snapshot.data == null) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return ListView.builder(
                  padding: EdgeInsets.all(20),
                  itemBuilder: (context, index) {
                    return MenuItem(
                      item: snapshot.data[index],
                    );
                  },
                  itemCount: snapshot.data.length,
                );
              },
            ),
          )),
    );
  }
}

class MenuItem extends StatelessWidget {
  final Product item;
  const MenuItem({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed("/product_details", arguments: {
              "item": item,
            });
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Hero(
                tag: item.id,
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(8.0),
                  child: Image.network(
                    item.image,
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                ),
              ),
              SizedBox(
                width: 16.0,
              ),
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 8.0,
                      ),
                      Text(
                        item.name,
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Container(
                          width: 200.0,
                          child: Text(
                            item.description,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                            style: TextStyle(color: Colors.grey),
                          )),
                    ],
                  ),
                ),
              ),
              Text("${item.options[item.options.keys.toList()[0]]} EGP")
            ],
          ),
        ));
  }
}
