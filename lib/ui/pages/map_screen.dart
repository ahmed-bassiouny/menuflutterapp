import 'dart:async';


import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';

class MapScreen extends StatefulWidget {
  @override
  State<MapScreen> createState() => MapScreenState();
}

class MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex;
  LatLng myTarget;
  
  String searchTxtBtn = "Search  by name";

  @override
  void initState() {
    super.initState();
    
    getCurrentPosition();
  }

  void getCurrentPosition() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (position.latitude == 0) {
      Toast.show("Please access location from setting", context);
      return;
    }
    myTarget = new LatLng(position.latitude, position.longitude);
    setState(() {
      _kGooglePlex = CameraPosition(
        target: LatLng(position.latitude, position.longitude),
        zoom: 14.4746,
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _kGooglePlex == null
          ? Center(
              child: FlatButton(
                child: Text("We Can't access your location"),
                onPressed: () {
                  getCurrentPosition();
                },
              ),
            )
          : Stack(
              children: <Widget>[
                GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: _kGooglePlex,
                  onCameraMove: (CameraPosition position){
                    setState(() {
                      myTarget = position.target;
                    });
                  },
                  onTap: (LatLng pos) {
                    setState(() {
                      myTarget = pos;
                    });
                  },
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  myLocationEnabled: true,
                  markers: Set<Marker>.of(<Marker>[
                    Marker(markerId: MarkerId("1"), position: myTarget)
                  ]),
                ),
              ],
            ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context, myTarget);
        },
        label: Text('Select'),
        icon: Icon(Icons.check),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
