import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/product_details_bloc.dart';
import 'package:menu_app/model/cart_item.dart';
import 'package:menu_app/model/product.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:menu_app/supportedFile/constant.dart';

class ProductDetails extends StatefulWidget {
  final Product item;

  ProductDetails({this.item});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final bloc = ProductDetailsBloc();
  var size = "";
  var listKey;
  @override
  void initState() {
    super.initState();
    Product item = widget.item;
    listKey = item.options.keys.toList();
    size = listKey[0];
    bloc.changeSize(item.options[size].toDouble(), size);
    bloc.setInfo(item.id, item.name, item.image,item.isOffer);
  }

  void _showModalSheet() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return ListView.builder(
            itemCount: listKey.length,
            itemBuilder: (ctx, index) {
              return ListTile(
                title: Text("${listKey[index]}"),
                trailing: Text("${widget.item.options[listKey[index]]}"),
                onTap: () {
                  size = listKey[index];
                  bloc.changeSize(
                      widget.item.options[listKey[index]].toDouble(),
                      listKey[index]);
                  Navigator.of(context).pop();
                },
              );
            },
            padding: EdgeInsets.all(4.0),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProductDetailsBloc>(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate("product_details")),
        ),
        body: Container(
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(12),
                  child: Hero(
                    tag: widget.item.id,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(20.0),
                      child: Image.network(
                        widget.item.image,
                        fit: BoxFit.cover,
                        width: 200,
                        height: 200,
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Divider(),
                      Text(
                        widget.item.name,
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25.0,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        widget.item.description,
                        style: TextStyle(color: Colors.grey, fontSize: 16.0),
                        textAlign: TextAlign.center,
                      ),
                      Divider(),
                    ],
                  ),
                ),
                StreamBuilder<CartItem>(
                  stream: bloc.productDetailsStream,
                  builder: (cxt, snapshot) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 12),
                          width: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Colors.indigoAccent),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.add),
                                onPressed: () {
                                  bloc.increament();
                                },
                                color: Colors.white,
                              ),
                              Text(
                                "${snapshot.data == null ? 0 : snapshot.data.count}",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                              IconButton(
                                icon: Icon(Icons.remove),
                                color: Colors.white,
                                onPressed: () {
                                  bloc.decreament();
                                },
                              ),
                            ],
                          ),
                        ),
                        FlatButton(
                          child: Text("${AppLocalizations.of(context).translate("change_size")} ($size)"),
                          onPressed: () {
                            _showModalSheet();
                          },
                        ),
                        Text(
                          "${AppLocalizations.of(context).translate("total")} : ${snapshot.data == null ? 0 : snapshot.data.total}",
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        FlatButton.icon(
                          color: Theme.of(context).primaryColor,
                          
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(12.0),
                              ),
                          onPressed: () {
                            Constant.cartList.add(bloc.item);
                            Navigator.of(context).pop();
                          },
                          label: Text(AppLocalizations.of(context).translate("add"),style: TextStyle(fontSize: 20),),
                          padding: EdgeInsets.fromLTRB(60, 12, 70, 12),
                          textColor: Colors.white,
                          icon: Icon(Icons.check),
                        ),SizedBox(
                          height: 20,
                        ),
                      ],
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
