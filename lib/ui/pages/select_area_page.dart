import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/city_area_bloc.dart';
import 'package:menu_app/model/area.dart';
import 'package:menu_app/model/city.dart';

class SelectAreaPage extends StatefulWidget {
  @override
  _SelectAreaPageState createState() => _SelectAreaPageState();
}

class _SelectAreaPageState extends State<SelectAreaPage> {
  final bloc = CityAreaBloc();
  List<City> cityList;
  List<Area> areaList;

  void _showCityModalSheet() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return ListView.builder(
            itemCount: cityList == null ? 0 :cityList.length,
            itemBuilder: (ctx, index) {
              return ListTile(
                title: Text("${cityList[index].name}"),
                onTap: () {
                  areaList = null;
                  bloc.setCity(cityList[index]);
                  bloc.startLoading();
                  bloc.fetchAreaData();
                  Navigator.of(context).pop();
                },
              );
            },
            padding: EdgeInsets.all(4.0),
          );
        });
  }

  void _showAreaModalSheet() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return ListView.builder(
            itemCount: areaList == null ? 0 : areaList.length,
            itemBuilder: (ctx, index) {
              return ListTile(
                title: Text("${areaList[index].name}"),
                onTap: () {
                  bloc.setArea(areaList[index]);
                  Navigator.of(context).pop();
                },
              );
            },
            padding: EdgeInsets.all(4.0),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    bloc.startLoading();
    bloc.fetchCityData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CityAreaBloc>(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Container(
              margin: EdgeInsets.all(20),
              child: StreamBuilder<CityAreaBlocHelper>(
                  stream: bloc.stream,
                  builder: (cxt, snapshot) {
                   return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          "Please Select Area",
                          style: TextStyle(fontSize: 20),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        TextField(
                          onTap: () {
                            if (snapshot.data != null) {
                              cityList = snapshot.data.cityList;
                              _showCityModalSheet();
                            }
                          },
                          readOnly: true,
                          decoration: InputDecoration(
                              hintText: snapshot.data !=  null && snapshot.data.city != null ? snapshot.data.city.name :"City",
                              prefixIcon: Icon(Icons.arrow_drop_down),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                    width: 1,
                                  ))),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        TextField(
                          onTap: () {
                            if (snapshot.data != null && snapshot.data.areaList != null) {
                              areaList = snapshot.data.areaList;
                              _showAreaModalSheet();
                            }
                          },
                          readOnly: true,
                          decoration: InputDecoration(
                              hintText: snapshot.data !=  null && snapshot.data.area != null ? snapshot.data.area.name :"Area",
                              prefixIcon: Icon(Icons.arrow_drop_down),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                    width: 1,
                                  ))),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        snapshot.data == null || snapshot.data.loading
                            ? CircularProgressIndicator()
                            : FlatButton(
                                child: Text("Save",style: TextStyle( fontSize: 18,color: Theme.of(context).primaryColor),),
                                onPressed: () {
                                  bloc.save(context);
                                },
                              )
                      ],
                    );
                  })),
        ),
      ),
    );
  }
}
