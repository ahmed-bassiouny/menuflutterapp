import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/home_bloc.dart';
import 'package:menu_app/main.dart';
import 'package:menu_app/model/category.dart';
import 'package:menu_app/model/category_offer.dart';
import 'package:menu_app/model/offer.dart';
import 'package:menu_app/model/product.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:menu_app/ui/pages/product_list.dart';
import 'package:menu_app/ui/supportedUI/MyAppbar.dart';
import 'package:polygon_clipper/polygon_clipper.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

final bloc = HomeBloc();

class _CategoryPageState extends State<CategoryPage> {
  @override
  void initState() {
    super.initState();
    bloc.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      bloc: bloc,
      child: Scaffold(
          appBar: MyAppbar(
            title: AppLocalizations.of(context).translate("home_title"),
          ),
          drawer: Drawer(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                    accountName: Text(
                      "Holems",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    accountEmail: Text(
                      "Holems@gmail.com",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    currentAccountPicture: Image.asset("images/logo.png"),
                    decoration: BoxDecoration(color: Colors.white)
                    //currentAccountPicture: ,
                    ),
                ListTile(
                  leading: Icon(Icons.history),
                  title: Text(AppLocalizations.of(context).translate("history")),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).pushNamed("/history");
                  },
                ),
                ListTile(
                  leading: Icon(Icons.list),
                  title: Text(AppLocalizations.of(context).translate("branchs")),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).pushNamed("/branch");
                  },
                ),
                ListTile(
                  leading: Icon(Icons.info),
                  title: Text(AppLocalizations.of(context).translate("about")),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).pushNamed("/about");
                  },
                )
              ],
            ),
          ),
          body: RefreshIndicator(
            child: Container(
                child: StreamBuilder<CategoryOffer>(
              stream: bloc.homeStream,
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return ListView(
                  children: <Widget>[
                    
                    
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(AppLocalizations.of(context).translate("offers"),style: TextStyle(color: Colors.black,fontSize: 20),),
                    ),
                    
                    FoodListview(
                      items: snapshot.data.offers,
                    ),
                    
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(AppLocalizations.of(context).translate("category"),style: TextStyle(color: Colors.black,fontSize: 20),),
                    ),
                    SelectTypeSection(
                      items: snapshot.data.categories,
                    ),
                    SizedBox(height: 16.0),
                  ],
                );
              },
            )),
            onRefresh: _handleRefresh,
          )),
    );
  }
}

Future<Null> _handleRefresh() async {
  bloc.fetchData();
  return null;
}

class SelectTypeSection extends StatelessWidget {
  final List<Category> items;
  const SelectTypeSection({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: ListView.builder(
            itemCount: items.length,
            primary: false,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/product_list", arguments: {
                    "id": items[index].id,
                    "title": items[index].name
                  });
                },
                child: Card(
                  elevation: 4,
                  child: Container(
                    height: 200.0,
                    width: 200.0,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.network(
                          items[index].image,
                          fit: BoxFit.fill,
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 8),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                Colors.black.withOpacity(0.1),
                                Colors.black.withOpacity(0.8),
                              ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter)),
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              child: Text(
                                items[index].name,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }));
  }
}

class FoodListview extends StatelessWidget {
  final List<Product> items;
  const FoodListview({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        height: 200.0,
        child: ListView.builder(
          itemCount: items.length,
          primary: false,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return ItemCard(item: items[index]);
          },
        ),
      ),
    );
  }
}

class ItemCard extends StatelessWidget {
  final Product item;
  const ItemCard({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed("/product_details", arguments: {
              "item": item,
            });
          },
          child: Container(
              height: 160.0,
              width: 300.0,
              child: Image.network(
                item.image,
                fit: BoxFit.cover,
              )),
        ));
  }
}
