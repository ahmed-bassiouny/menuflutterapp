import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/cart_bloc.dart';
import 'package:menu_app/model/cart_item.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:menu_app/supportedFile/constant.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

Widget itemCart(CartBloc bloc, CartItem item, int index) {
  return ListTile(
    leading: ClipRRect(
      borderRadius: new BorderRadius.circular(8.0),
      child: Image.network(
        item.image,
        fit: BoxFit.cover,
        width: 80,
        height: 100,
      ),
    ),
    title: Text("${item.title}(${item.size})"),
    trailing: Text("${item.total} EG"),
    subtitle: SizedBox(
      width: 20,
      height: 40,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle_outline),
            onPressed: () {
              bloc.increament(index);
            },
            iconSize: 20,
            color: Colors.black,
          ),
          Text(
            "${item.count}",
            style: TextStyle(color: Colors.black, fontSize: 18),
          ),
          IconButton(
            icon: Icon(Icons.remove_circle_outline),
            color: Colors.black,
            iconSize: 20,
            onPressed: () {
              bloc.decreament(index);
            },
          ),
        ],
      ),
    ),
  );
}

class _CartPageState extends State<CartPage> {
  final bloc = CartBloc();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CartBloc>(
        bloc: bloc,
        child: Scaffold(
          appBar: AppBar(
            title: Text(AppLocalizations.of(context).translate("cart")),
          ),
          body: StreamBuilder<List<CartItem>>(
            stream: bloc.cartStream,
            builder: (ctx, snapshot) {
              return ListView.separated(
                itemCount: snapshot.data != null ? snapshot.data.length : 0,
                padding: EdgeInsets.fromLTRB(8, 20, 8, 20),
                separatorBuilder: (BuildContext context, int index) =>
                    Divider(),
                itemBuilder: (context, index) {
                  return itemCart(bloc, snapshot.data[index], index);
                },
              );
            },
          ),
          floatingActionButton: StreamBuilder<double>(
            stream: bloc.totalStream,
            builder: (cxt, snapshot) {
              return FloatingActionButton.extended(
                onPressed: () {
                  if (snapshot.data != null && snapshot.data > 0) {
                    Navigator.of(context).pushNamed("/order", arguments: {
                      "item": snapshot.data,
                    });
                  }
                },
                icon: Icon(Icons.check),
                label: Text("${AppLocalizations.of(context).translate("total")} (${snapshot.data} EG)"),
              );
            },
          ),
        ));
  }
}
