import 'package:flutter/material.dart';
import 'package:menu_app/bloc/base/bloc_provider.dart';
import 'package:menu_app/bloc/branch_bloc.dart';
import 'package:menu_app/bloc/history_bloc.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/order.dart';
import 'package:menu_app/supportedFile/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  final bloc = HistoryBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc.fetchData();
  }

  String getItems(List<OrderItem> items){
    var res = "";
    for (var item in items) {
      res += item.name + ", " ;
    }
    return res;
  }
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HistoryBloc>(
      bloc: bloc,
      child: Scaffold(
          appBar: AppBar(
            title: Text(AppLocalizations.of(context).translate("history")),
          ),
          body: StreamBuilder<List<Order>>(
            stream: bloc.ordersStream,
            builder: (ctx, snapshot) {
              return snapshot.data == null
                  ? Center(child:CircularProgressIndicator())
                  : ListView.builder(
                      itemCount:
                          snapshot.data == null ? 0 : snapshot.data.length,
                      itemBuilder: (ctx, index) {
                        return Card(
                          child: Container(
                            padding: EdgeInsets.all(8),
                            child: ListTile(
                              title: Text("${snapshot.data[index].name} - ${snapshot.data[index].phone}"),
                              subtitle: Text(getItems(snapshot.data[index].items)),
                              trailing: FlatButton(
                                child: Text(
                                  "View Location",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                                onPressed: () async {
                                  final url =
                                      'https://www.google.com/maps/search/?api=1&query=${snapshot.data[index].location.latitude},${snapshot.data[index].location.longitude}';
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    throw 'Could not launch';
                                  }
                                },
                              ),
                            ),
                          ),
                        );
                      });
            },
          )),
    );
  }
}
