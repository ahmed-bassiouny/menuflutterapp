
import 'dart:io';
import 'dart:ui';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:menu_app/model/area.dart';
import 'package:menu_app/model/branch.dart';
import 'package:menu_app/model/cart_item.dart';
import 'package:menu_app/model/promo_code.dart';

class Constant{

  static List<CartItem> cartList = List<CartItem>();

  static String DEVICE_ID = "";
  static String DEVICE_ID_KEY = "device_key";

  static String DEVICE_TYPE = Platform.isAndroid ? "android":"ios";

  static Area REGION = null;
  static String REGION_KEY = "region_key";
  static List<Branch> branchList;
  static String locale = "";

  static Future<String> getDeviceDetails() async {
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        identifier = build.androidId;  //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        identifier = data.identifierForVendor;  //UUID for iOS
      }
    } on PlatformException catch(e) {
      print('Failed to get platform version');

    }

//if (!mounted) return;
return identifier;
}
  
}